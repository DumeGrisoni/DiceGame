// Declaration des variables
let global = 0, score = 0, id = 1;

// Logique du Roll
let rollBtn = document.querySelector(".roll").addEventListener('click', () =>{
    // Recherche d'un chiffre en 1 et 6 
    let randomNumber = Math.floor(Math.random() * 6) + 1;
    // Recherche de l'image grâce à la variable RandomNumber
    let randomDice = document.querySelector(".dice-img").src = './images/dice' + randomNumber + ".png";

    // Logique des points
    if(randomNumber !== 1 ){
      score += randomNumber;
      document.getElementById("current" + id).textContent = score;
   }else {
     togglePlayer();

  };
});


// Bouton Hold
let holdBtn = document.querySelector(".hold").addEventListener("click", () =>{

    global += score;
    score = 0
    document.getElementById("global"+ id).textContent = global;
    //Verification si le joueur a atteint 100
    if(global>= 100){
      newGame();
    }else {   
    togglePlayer();
    }
});


// Bouton NewGame
let newGame = ()=>{
  //Reinitialisation des variables
    id = 1;
    score = 0;
    global = 0;
    document.querySelector(".left").textContent = 'Your Turn';
    document.querySelector(".right").textContent = null;

    document.querySelector(".current-point1").textContent = score;
    document.querySelector(".current-point2").textContent = score;
    document.getElementById("global1").textContent = global;
    document.getElementById("global2").textContent = global;
    document.querySelector(".left").classList.remove(id);
    document.querySelector(".left").classList.add(id);
    document.querySelector(".right").classList.remove(id);
  
    
} ;
document.querySelector('.newGame').addEventListener("click", newGame);


//Toogle de joueurs 
let togglePlayer = () => {
  score = 0;
  document.getElementById("current" + id).textContent = score;
  if (id === 1 ) {
    document.querySelector(".left").textContent = null;
    id = 2;
    document.querySelector(".right").textContent = 'Your Turn';
  }else {
    document.querySelector(".right").textContent = null;
    id = 1;
    document.querySelector(".left").textContent = 'Your Turn';
  };
  
};


newGame();
