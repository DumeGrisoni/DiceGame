Dice Game

Jeu de dès ou le premier joueur qui atteint les 100 points a gagner.
pour celà le joueur doit clicker sur le bouton Roll Dice, si son jet est different de 1 les points se cumulent dans la zone current.
Si au contraire le joueur réalise un 1 les points accumulés jusque là seront réinitialisés.
Le joueur peut clicker sur Hold pour sauvegarder ses points.

Variables :
Utilisation de variables:

- score pour le current
- global pour les points sauvegardés avec le Hold
- id permettant de choisir le joueur en lui assignant cet id après le nom de classe lors d'un selecteur
